package ucrypt

import (
	"errors"
	"fmt"
	"runtime"
	"strconv"
	"strings"

	"golang.org/x/crypto/argon2"
)

func init() {
	UCryptImplementations[ucryptArgonID] = &UCryptArgon{Time: ArgonDefaultTime, Memory: ArgonDefaultMemory, Mode: ArgonDefaultMode, HashSize: ArgonDefaultHashSize}
}

const ucryptArgonID = "argon2"
const bitSize = 32

// Default scrypt values
const (
	ArgonDefaultTime     = 4
	ArgonDefaultMemory   = 32 * 1024
	ArgonDefaultHashSize = 32
	ArgonDefaultMode     = "id"
)

// ArgonThreads defines the number of threads password gen is able to use
var ArgonThreads = uint8(runtime.NumCPU() / 2)

// UCryptArgon is used to configure cost and size parameters.
type UCryptArgon struct {
	Time     uint32
	Memory   uint32
	Mode     string
	HashSize int // hash size in bytes (min. 16)
}

func (uc *UCryptArgon) encodedString() string {
	return fmt.Sprintf("%s,%d,%d", uc.Mode, uc.Time, uc.Memory)
}

// IsValid returns true if the config parameters are valid.
//
// The performed validation is machine-independent, which means that IsValid
// can return true on both 32-bit and 64-bit machines, however the actual
// password hashing can fail with error on the former, but not on the latter.
func (uc *UCryptArgon) IsValid() bool {
	if uc == nil {
		return false
	}
	if uc.Mode != "i" && uc.Mode != "id" {
		return false
	}
	if uc.Time*uc.Memory >= maxRP {
		return false
	}

	return true
}

// Encrypt encrypts the password with the specified salt
func (uc *UCryptArgon) Encrypt(password, salt []byte) (string, []byte, error) {
	// Validate config.
	if !uc.IsValid() {
		return "", nil, ErrBadHashFormat
	}

	var h []byte
	var err error

	switch uc.Mode {
	case "i":
		h = argon2.Key(password, salt, uc.Time, uc.Memory, ArgonThreads, uint32(uc.HashSize))
	case "id":
		h = argon2.IDKey(password, salt, uc.Time, uc.Memory, ArgonThreads, uint32(uc.HashSize))
	default:
		err = errors.New("unkown hashmode")
	}

	if err != nil {
		return "", nil, err
	}

	// Encode.
	return uc.encodedString(), h, nil
}

// Configure creates a copy of the base implementation with all parameteres applied
func (uc *UCryptArgon) Configure(params string, hashSize byte) (hasher UCryptImplementation, err error) {
	nc := *uc
	if hashSize > 0 {
		nc.HashSize = int(hashSize)
	}
	values := strings.Split(params, ",")
	nc.Mode = values[0]

	temp, err := strconv.ParseUint(values[1], 10, bitSize)
	if err != nil {
		return nil, err
	}
	nc.Time = uint32(temp)

	temp, err = strconv.ParseUint(values[2], 10, bitSize)
	if err != nil {
		return nil, err
	}
	nc.Memory = uint32(temp)

	return &nc, nil
}

func (uc UCryptArgon) String() string {
	return fmt.Sprintf("mode:%s time:%d memory:%d", uc.Mode, uc.Time, uc.Memory)
}
