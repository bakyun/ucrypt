package ucrypt

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/crypto/scrypt"
)

func init() {
	UCryptImplementations[ucryptScryptID] = &UCryptScrypt{LogN: ScryptDefaultLogN, R: ScryptDefaultR, P: ScryptDefaultP, HashSize: ScryptDefaultHashSize}
}

const ucryptScryptID = "scrypt"

// Default scrypt values
const (
	ScryptDefaultLogN     = 14
	ScryptDefaultR        = 8
	ScryptDefaultP        = 1
	ScryptDefaultHashSize = 32
)

const (
	maxRP = 1 << 30
)

// UCryptScrypt is used to configure cost and size parameters.
type UCryptScrypt struct {
	LogN int8 // CPU/memory cost (between 1 and 63)
	R    int  // block size (min. 1, must satisfy (R*P) < 2³⁰)
	P    int  // parallelization (min. 1, must satisfy (R*P) < 2³⁰)

	SaltSize int // salt size in bytes
	HashSize int // hash size in bytes (min. 16)
}

func (uc *UCryptScrypt) encodedString() string {
	return fmt.Sprintf("%d,%d,%d", uc.LogN, uc.R, uc.P)
}

// IsValid returns true if the config parameters are valid.
//
// The performed validation is machine-independent, which means that IsValid
// can return true on both 32-bit and 64-bit machines, however the actual
// password hashing can fail with error on the former, but not on the latter.
func (uc *UCryptScrypt) IsValid() bool {
	if uc == nil {
		return false
	}
	if uc.LogN <= 0 || uc.LogN > maxLogN {
		return false
	}
	if uc.R <= 0 || uc.P <= 0 {
		return false
	}
	if uint64(uc.R)*uint64(uc.P) >= maxRP {
		return false
	}
	if uc.HashSize < minHashSize {
		return false
	}
	return true
}

// Encrypt encrypts the password with the specified salt
func (uc *UCryptScrypt) Encrypt(password, salt []byte) (string, []byte, error) {
	// Validate config.
	if !uc.IsValid() {
		return "", nil, ErrBadHashFormat
	}

	// Make sure N won't overflow int.
	N := 1 << uint64(uc.LogN)
	if N > maxInt {
		return "", nil, errors.New("logN is too large")
	}

	// Calculate hash.
	h, err := scrypt.Key(password, salt, int(N), uc.R, uc.P, uc.HashSize)
	if err != nil {
		return "", nil, err
	}

	// Encode.
	return uc.encodedString(), h, nil
}

// Configure creates a copy of the base implementation with all parameteres applied
func (uc *UCryptScrypt) Configure(params string, hashSize byte) (hasher UCryptImplementation, err error) {
	nc := *uc
	if hashSize > 0 {
		nc.HashSize = int(hashSize)
	}
	values := strings.Split(params, ",")
	logN, err := strconv.ParseInt(values[0], 10, 8)
	if err != nil {
		return nil, err
	}
	nc.LogN = int8(logN)

	nc.R, err = strconv.Atoi(values[1])
	if err != nil {
		return nil, err
	}

	nc.P, err = strconv.Atoi(values[2])
	if err != nil {
		return nil, err
	}

	return &nc, nil
}

func (uc UCryptScrypt) String() string {
	return fmt.Sprintf("n:%d r:%d p:%d", 1<<uint64(uc.LogN), uc.R, uc.P)
}
