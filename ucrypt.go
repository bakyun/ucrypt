package ucrypt

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"strings"
)

const (
	maxLogN = 63
	maxInt  = int(^uint(0) >> 1)

	minHashSize     = 16
	hashParamLength = 5
)

const (
	// DefaultSaltSize is the default size for salts
	DefaultSaltSize = 32
)

var (
	// UCryptImplementations hold all defined crypt implementations
	UCryptImplementations = make(map[string]UCryptImplementation)

	// SaltSize defines the bytes used for salts
	SaltSize = DefaultSaltSize
)

// Errors.
var (
	ErrUnkownHasher              = errors.New("unkown hasher")
	ErrMismatchedHashAndPassword = errors.New("hash is not the hash of the given password")
	ErrBadHashFormat             = errors.New("wrong hash format")
)

// UCryptImplementation is the crypting interface
type UCryptImplementation interface {
	Encrypt(password, salt []byte) (encodedParams string, key []byte, err error)
	Configure(string, byte) (UCryptImplementation, error)
}

func hmacKey(params string, key []byte) ([]byte, error) {
	hm := hmac.New(sha256.New, []byte(params))
	if _, err := hm.Write(key); err != nil {
		return nil, err
	}
	sum := hm.Sum(nil)

	return sum, nil
}

// HashPassword returns the hash of the password with the given configuration.
// If config is nil, DefaultConfig is used.
//
// To compare the returned hash with the password later, use VerifyPassword.
func HashPassword(impl string, password []byte, config string, useHashSize byte) (string, error) {
	hasher, found := UCryptImplementations[impl]
	if !found {
		return "", ErrUnkownHasher
	}

	if config != "" {
		var err error
		hasher, err = hasher.Configure(config, useHashSize)
		if err != nil {
			return "", ErrBadHashFormat
		}
	}

	// Generate new random salt.
	salt := make([]byte, SaltSize)
	if _, err := io.ReadFull(rand.Reader, salt[:]); err != nil {
		return "", err
	}

	// Hash password.
	params, hash, err := hasher.Encrypt(password, salt[:])
	if err != nil {
		return "", err
	}

	hashSize := byte(len(hash))
	salt = append([]byte{hashSize}, salt...)

	encodedSalt := base64.StdEncoding.EncodeToString(salt)

	prefix := fmt.Sprintf("$%v$%v$%v$", impl, params, encodedSalt)
	hmacHash, err := hmacKey(prefix, hash)
	if err != nil {
		return "", err
	}

	encodedHash := base64.StdEncoding.EncodeToString(hmacHash)

	return prefix + encodedHash, nil
}

// VerifyPassword compares the given hash with the hash
// of the given password and returns nil on success, or an error on failure.
func VerifyPassword(hash string, password []byte) (bool, error) {
	params := strings.Split(hash, "$")

	// {_}$
	if len(params) != hashParamLength || params[0] != "" {
		return false, ErrBadHashFormat
	}

	// _${impl}
	hasher, found := UCryptImplementations[params[1]]
	if !found {
		return false, ErrUnkownHasher
	}

	// _$impl$params${salt}
	salt, err := base64.StdEncoding.DecodeString(params[3])
	if err != nil {
		return false, err
	}

	hashSize := salt[0]
	salt = salt[1:]

	// _$impl${params}
	configuredHasher, err := hasher.Configure(params[2], hashSize)
	if err != nil {
		return false, err
	}

	// generate hash
	_, key, err := configuredHasher.Encrypt(password, salt)
	if err != nil {
		return false, err
	}

	// hmac the key
	hashed, err := hmacKey(hash[:len(hash)-len(params[4])], key)
	if err != nil {
		return false, err
	}

	// _$impl$params$salt${key}
	baseMac, err := base64.StdEncoding.DecodeString(params[4])
	if err != nil {
		return false, err
	}
	return hmac.Equal(baseMac, hashed), nil
}
